# jupyterhub

This repo deploys [jupyterhub](https://zero-to-jupyterhub.readthedocs.io/en/latest/jupyterhub/installation.html).

## Admin access

The first user signing up with the username `admin` can chose a password.

## Signup

New users signing up need to authorized by an admin.

Open http://jupyterhub.[domain]/hub/authorize to do so.

## Change password

To change your own password (loged in) open http://jupyterhub.[domain]/hub/change-password.

To reset a users password (as an admin) open http://jupyterhub.[domain]/hub/change-password/[username].
